from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    url="https://api.pexels.com/v1/search"
    headers={
        "Authorization":PEXELS_API_KEY,
    }
    params={
        "query":f'{city}{state}',
        "per_page":1,
    }
    try:
        response=requests.get(url,headers=headers, params=params)
        response.raise_for_status()
        data=response.json()

        if data.get("photos") and len(data["photos"])>0:
            pic_url=data["photos"][0]["src"]["original"]
            return {'pic_url':pic_url}
        else:
            return None
    except requests.RequestException as e:
        print(f"Error making photo API request: {e}")
        return None

def get_weather_data(city,state):
    api_key='OPEN_WEATHER_API_KEY'
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&limit={1}&appid={api_key}"
    try:
        response=requests.get(url)
        response.raise_for_status()
        data=response.json()
        latitude=data[0].get('lat')
        longitude=data[0].get('lon')
        weather_url="https://api.openweathermap.org/data/2.5/weather"
        params={
            'lat':latitude,
            'lon':longitude,
            'appid': api_key,
        }
        weather_response=requests.get(weather_url, params=params)
        weather_response.raise_for_status()
        weather_data=weather_response.json()

    #I feel pretty stuck after this part...
    except requests.RequestException as e:
        print(f"Error in making weather API request: {e}")
        return None
